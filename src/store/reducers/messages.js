import {GET_MESSAGES_SUCCESS} from "../actions/actionTypes";

const initialState = {
  messages: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MESSAGES_SUCCESS:
      return {...state, messages: action.messages};
    default:
      return state;
  }
};

export default reducer;