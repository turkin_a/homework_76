import {GET_MESSAGES_SUCCESS} from "./actionTypes";
import axios from '../../axios';

export const getMessagesSuccess = (data) => {
  return {type: GET_MESSAGES_SUCCESS, messages: data}
};

export const getMessages = () => {
  return dispatch => {
    axios.get('/messages').then(res => {
      dispatch(getMessagesSuccess(res.data));
    });
  }
};