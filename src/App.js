import React, { Component } from 'react';
import './App.css';
import SendMessages from "./containers/SendMessages/SendMessages";
import ListOfMessages from "./containers/ListOfMessages/ListOfMessages";
import {connect} from 'react-redux';
import {getMessages} from "./store/actions/actions";

class App extends Component {
  componentDidMount() {
    setInterval(() => {
      this.props.getMessages();
    }, 2000);
  }


  render() {
    return (
      <div className="container" id="App">
        <h4>Chat</h4>
        <ListOfMessages messages={this.props.messages}/>
        <SendMessages/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {messages: state.messages};
};

const mapDispatchToProps = dispatch => {
  return {getMessages: () => dispatch(getMessages())};
};



export default connect(mapStateToProps, mapDispatchToProps)(App);
