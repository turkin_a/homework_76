import React from 'react';
import {Button, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import './SendMessages.css';
import axios from '../../axios';

class SendMessages extends React.Component {
  state = {
    name: '',
    message: ''
  };

  changeDataHandler = event => {
    this.setState({[event.target.name]: event.target.value})
  };

  sendData = e => {
    e.preventDefault();
    axios.post('/messages', this.state).then(() => {
      this.setState({name: '', message: ''});
    }).catch(error => {
      console.log(error);
    })
  };

  render (){
    return (
      <Form inline>
        <FormGroup  controlId="formInlineName">
          <ControlLabel>Name</ControlLabel>
          <FormControl className="name-input" required type="text" placeholder="Ravshan" name="name" onChange={this.changeDataHandler} value={this.state.name}/>
        </FormGroup>
        <FormGroup>
          <ControlLabel className="message">Message</ControlLabel>
          <FormControl className="message-input" required type="text" placeholder="Enter your message" name="message" onChange={this.changeDataHandler} value={this.state.message}/>
        </FormGroup>
        <Button type="submit" onClick={this.sendData}>Send</Button>
      </Form>
    )
  }
}

export default SendMessages;