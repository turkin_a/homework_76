import React from 'react';
import {ListGroup, ListGroupItem} from "react-bootstrap";
import './ListOfMessages.css';
import {connect} from 'react-redux';

const ListOfMessages = props => {
  return (
    <ListGroup >
      {props.messages.map(message => (
        <ListGroupItem key={message.id}>
          <h5 className="name">{message.name}</h5>
          <p>{message.date}</p>
          {message.message}
        </ListGroupItem>
        )
      )}
    </ListGroup>
  )
};

export default ListOfMessages;